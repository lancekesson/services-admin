# Requirements for Admin Tool Epic

### Admin tool to do modification IBS orders that have the wrong name

> As a administartor

> I need to be able to change the names of the occupants within IBS hotel orders.

> So that the correct names can be connected to the room/voucher



> Given that the names associated with a room are incorrect for a IBS order

> When I change the names and submit

> Then the changes should be displayed/represented on the IBS order

**Acceptance Criteria:**


**Stories:**

Given that there is an existing IBS order

When I access the order

Then the current information of the order should be displayed


	Tasks
	1. Create project and Repo
	2. install ReactJs
		- https://github.com/facebook/create-react-app
	3. Wireframe the GUI
	4. Create form
	

Given that that the app is running and the form has been developed

When the form is displayed

Then a unit test verfies that the form is displayed


	Tasks
	1. Setup Jest
		- https://jestjs.io/docs/en/tutorial-react
	2. Setup initial unit test
	3. Create unit test to test display of form componenet


Question:
What information needs to be displayed

Given that the Admin tool displays a form

When the form is submitted

Then a request is sent to IBS


	Tasks 
	1. Create interaction between form and IBS
		- Order number field
	2. Locally store the response.

Given that a response has been sent to IBS

When the response is received

Then the response is displayed to the GUI

	Task
	1. Display the response form IBS


Given that an IBS order is displayed

When the order details are displayed

Then each field should be editable


	Task
	1. Define the fields/properties within the response that are required to editable
	2. Wireframe the editable fields
	3. Create the editable fields(components)

Question:
What fields do we need to edit

Given that we can edit the order displayed

When I submit the changes

Then it makes the change on the IBS system


	Tasks
	1. Build the request from the edited fields 
	2. Submit the edited changes

Given that a change has been submitted to IBS

When the response is returned from IBS

Then approriate notification is displayed.

	Tasks
	1. Capture the response from IBS Res Modifiction
	2. Display Response notifaction to screens
	3. Display the read response to screen










