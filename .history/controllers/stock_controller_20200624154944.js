stockService = require('../services/stock_service');

class StockController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }

    async getStockReceipt(req, res, next) {
      const supplierOrderRef = req.params.supplierId;

      let stockReceipt = stockService.getStockReceipt(supplierOrderRef);
      console.log(stockReceipt);
      console.log('after';)
      res.send(stockReceipt);

    }

}

module.exports = new StockController;