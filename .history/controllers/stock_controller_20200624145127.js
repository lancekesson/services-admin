stockService = require('../services/stock_service');

class StockController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }

    getStockReceipt(req, res, next) {
      const supplierOrderRef = req.params.supplierId;

      stockReceipt = stockService.getStockReceipt(supplierOrderRef);
      console.log(stockReceipt);

      res.send(stockReceipt);

    }

}

module.exports = new StockController;