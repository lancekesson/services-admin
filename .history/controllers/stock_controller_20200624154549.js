stockService = require('../services/stock_service');

class StockController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }

    async getStockReceipt(req, res, next) {
      const supplierOrderRef = req.params.supplierId;

      let stockReceipt = await stockService.getStockReceipt(supplierOrderRef);
      console.log(stockReceipt);

      res.send(stockReceipt);

    }

}

module.exports = new StockController;