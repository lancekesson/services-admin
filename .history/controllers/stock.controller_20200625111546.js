stockService = require('../services/stock_service');
orderProductService = require('../services/order_product_service');

class StockController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }

    async getStockReceipt(req, res, next) {
      const supplierOrderRef = req.params.supplierId;

      let stockReceipt = await stockService.getStockReceipt(supplierOrderRef);
      console.log(stockReceipt);
      console.log('after');
      res.send(stockReceipt);

    }

    async updateStock(req, res, next) {
      const orderId = req.params.orderId;
      let orderProduct = await orderProductService.getOrderProduct(orderId);


      res.send(orderProduct);

    }


}

module.exports = new StockController;