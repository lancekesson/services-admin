stockService = require('../services/stock.service');
orderProductService = require('../services/order_product.service');

class StockController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }

    async getStockReceipt(req, res, next) {
      const supplierOrderRef = req.params.supplierId;

      let stockReceipt = await stockService.getStockReceipt(supplierOrderRef);
      console.log(stockReceipt);
      console.log('after');
      res.send(stockReceipt);

    }

    async getStockReceiptLines(req, res, next) {
      const stockReceiptId = req.params.stockReceiptId;

      let stockReceiptLines = await stockService.getStockReceiptLines(stockReceiptId);
      console.log(stockReceiptLines);
      console.log('after');
      res.send(stockReceiptLines);

    }

    async updateStock(req, res, next) {
      const orderId = req.params.orderId;
      let orderProduct = await orderProductService.getOrderProduct(orderId);


      res.send(orderProduct);

    }


}

module.exports = new StockController;