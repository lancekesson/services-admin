readWriteFile = require('../src/services/ibs_modification/read_write_file.service.js');
readModUpdate = require('../src/services/ibs_modification/read_mod_update.service.js');
const path = require('path');
let Request = require("request");

readModUpdateController = require('../src/controllers/ibs/read_mod_update.controller');

const readModRoutes = (app, fs) => {

    // READ
    app.get('/readmod', readModUpdateController.doReadWriteNameMod);

    //READ
    app.get('/api/readmod', readModUpdateController.doApiReadWriteNameMod);

};

module.exports = readModRoutes;