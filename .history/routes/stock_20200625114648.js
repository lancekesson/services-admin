const stockController = require('../src/controllers/stock.controller');

const stockRoutes = (app, fs) => {

    // variables
    let collectedModData = [];

    // READ
    app.get('/api/stock/receipts/:supplierId',  stockController.getStockReceipt);

    // READ
    app.get('/api/stock/receipt-lines/:stockReceiptId',  stockController.getStockReceiptLines);

    // READ
    app.get('/api/stock/items/:stockReceiptLineId',  stockController.getStockItems);

    // READ
    app.get('/api/stock/items-use/:stockItemId',  stockController.getStockItemUsage);

    //UPDATE
    app.get('/stock/update/:orderId',  stockController.updateStock);
     
};

module.exports = stockRoutes;