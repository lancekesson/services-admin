const readModUpdateController = require('../src/controllers/ibs/read_mod_update.controller')

const readModRoutes = (app, fs) => {
  // READ
  app.get('/readmod', readModUpdateController.doReadWriteNameMod)

  // READ
  app.get('/api/readmod/:orderId', readModUpdateController.doApiReadWriteNameMod)
}

module.exports = readModRoutes
