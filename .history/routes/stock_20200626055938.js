const stockController = require('../src/controllers/stock.controller');

const stockRoutes = (app, fs) => {

    // variables
    let collectedModData = [];

    // READ
    app.get('/api/stock/receipts/:supplierId',  stockController.getStockReceipt);

    // READ
    app.get('/api/stock/receipt-lines/:stockReceiptId',  stockController.getStockReceiptLines);

    // READ
    app.get('/api/stock/items/:stockReceiptLineId',  stockController.getStockItems);

    // READ
    app.get('/api/stock/items-use/:stockItemId',  stockController.getStockItemUsage);
    // CREATE
    app.post('/api/stock/items-use',  stockController.addStockItemUsage);

    // READ
    app.get('/api/stock/return-line/:orderId',  stockController.getReturnLine);
    // CREATE
    app.post('/api/stock/return-line',  stockController.addReturnLine);

    //UPDATE
    app.get('/stock/update/:orderId',  stockController.updateStock);
     
};

module.exports = stockRoutes;