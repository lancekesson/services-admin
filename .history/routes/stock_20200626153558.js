const stockStatusController = require('../src/controllers/stock/stock_status.controller');

const stockRoutes = (app, fs) => {


    //UPDATE
    app.get('/stock/check-status/:confId',  stockStatusController.checkStock);
     
};

module.exports = stockRoutes;