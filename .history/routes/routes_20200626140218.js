// load up our shiny new route for users
const readModRoutes = require('./read-mod');
const stockRoutes = require('./stock-api');
const stockRoutes = require('./stock');

const appRouter = (app, fs) => {

    // we've added in a default route here that handles empty routes
    // at the base API url
    app.get('/', (req, res) => {
        res.send('welcome to the development api-server');
    });


    // run our user route module here to complete the wire up
    readModRoutes(app, fs);

    // run our user route module here to complete the wire up
    stockApiRoutes(app, fs);

    stockRoutes(app, fs);

};

module.exports = appRouter;