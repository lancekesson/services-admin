const stockController = require('../controllers/stock_controller');

const stockRoutes = (app, fs) => {

    // variables
    let collectedModData = [];

    // READ
   // app.get('/stock/update/:orderId',  stockController.updateStock);

    app.get('/api/stock/receipts/:supplierId',  stockController.getStockReceipt);
        
};

module.exports = stockRoutes;