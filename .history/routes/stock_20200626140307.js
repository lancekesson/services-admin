const stockController = require('../src/controllers/stock/stock.controller');
const stockReceiptLineController = require('../src/controllers/stock/stock_receipt_line.controller');

const stockRoutes = (app, fs) => {


    //UPDATE
    app.get('/stock/check-status/:orderId',  stockController.updateStock);
     
};

module.exports = stockRoutes;