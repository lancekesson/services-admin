const stockController = require('../controllers/stock_controller');

const stockRoutes = (app, fs) => {

    // variables
    let collectedModData = [];

    // READ
    app.get('/api/stock/receipts/:supplierId',  stockController.getStockReceipt);

    //UPDATE
    app.get('/stock/update/:orderId',  stockController.updateStock);
     
};

module.exports = stockRoutes;