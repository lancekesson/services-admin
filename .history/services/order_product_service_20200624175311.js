orderProductModel = require('../models/order_product_model');

class OrderProductService {

    constructor() {
        
    }

    async getOrderProduct(orderId) {

        const orderProducts = await this.getOrderProductData(orderId);
        const filteredOrderProducts= this.filterOrderProducts(orderProducts);

        return filteredOrderProducts;
        
    }

    filterOrderProducts(orderProducts) {

        let filteredOrderProducts = new Map();
        let newOrderProducts = orderProducts.filter(orderProduct => {
            if(filteredOrderProducts.has(orderProduct.nid)) {

            } else {
                filteredOrderProducts.set(orderProduct.nid, [orderProduct]);
            }
        })
        return newOrderProducts;
    }

    async getOrderProductData(orderId) {

        return await new Promise(function (resolve, reject) {
            orderProductModel.getOrderProduct(orderId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Order Product with id ${orderId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Order Product with id " + orderId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }
            });
        });
    }


    
}

module.exports = new OrderProductService;