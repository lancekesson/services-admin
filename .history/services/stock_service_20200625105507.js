stockModel = require('../models/stock_receipt_model');

class StockService {

    constructor() {
        
    }

    async getStockReceipt(supplierOrderRef) {

        return await new Promise(function (resolve, reject) {

            stockModel.getStockReceipt(supplierOrderRef, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Receipt  with id ${supplierOrderRef}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Stock Receipt with id " + supplierOrderRef);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }
            });
        });
    }
    
}

module.exports = new StockService;

const StockReceipt = require("../models/stock/stock_receipt.model");
const StockReceiptLine = require('../models/stock/stock_receipt_line.model');
const StockItem = require('../models/stock/stock_item.model');
const StockItemUse = require('../models/stock/stock_item_uses.model');

// constructor
class StockService {
    //console.log('in constructor');
    constructor() {

    }

    async getStockReceipt(supplierOrderRef) {

        return await new Promise(function (resolve, reject) {

            StockReceipt.findBy(supplierOrderRef, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Customer with id ${supplierOrderRef}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Customer with id " + supplierOrderRef);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async getStockReceiptLine(stockReceiptId) {
        return await new Promise(function (resolve, reject) {

            StockReceiptLine.findBy(stockReceiptId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Receipt with id ${stockReceiptId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Stock Receipt with id " + stockReceiptId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async getStockItem(stockReceiptLineId) {
        return await new Promise(function (resolve, reject) {

            StockItem.findBy(stockReceiptLineId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Item with id ${stockReceiptLineId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Stock Item with id " + stockReceiptLineId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async getStockItemUse(stockItemId) {
        await new Promise(function (resolve, reject) {

            StockItemUse.findBy(stockItemId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Item Use with id ${stockItemId}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Stock Item Use with id " + stockItemId);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

}

module.exports = StockService;