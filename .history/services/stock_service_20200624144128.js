stockModel = require('../models/stock_model');

class StockService {

    constructor() {
        
    }

    getStockReceipt(supplierOrderRef) {
        return stockModel.getStockReceipts(supplierOrderRef);
    }
}

module.exports = new StockService;