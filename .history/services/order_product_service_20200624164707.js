orderProductModel = require('../models/order_product_model');

class OrderProductService {

    constructor() {
        
    }

    async getOrderProduct(orderId) {

        return await new Promise(function (resolve, reject) {
console.log(orderProductModel);
            orderProductModel.getOrderProduct(orderId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Order Product with id ${orderId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Order Product with id " + orderId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }
            });
        });
    }
    
}

module.exports = new OrderProductService;