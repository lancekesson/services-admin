stockModel = require('../models/stock_model');

class StockService {

    constructor() {
        
    }

    getStockReceipt(supplierOrderRef) {
        return stockModel.getStockReceipt(supplierOrderRef);
    }
}

module.exports = new StockService;