stockModel = require('../models/stock_model');

class StockService {

    constructor() {
        
    }

    async getStockReceipt(supplierOrderRef) {

        return await stockModel.getStockReceipt(supplierOrderRef);
    }
}

module.exports = new StockService;