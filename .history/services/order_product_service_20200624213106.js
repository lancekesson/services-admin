orderProductModel = require('../models/order_product_model');

class OrderProductService {

    constructor() {
        
    }

    async getOrderProduct(orderId) {

        const orderProducts = await this.getOrderProductData(orderId);
        const filteredOrderProducts = this.filterOrderProducts(orderProducts);
console.log(filteredOrderProducts);
        return filteredOrderProducts;
        
    }

    filterOrderProducts(orderProducts) {

        let filteredOrderProducts = new Map();
        for(let orderProduct of orderProducts) {
            if(filteredOrderProducts.has(orderProduct.nid)) {
                let tmp = filteredOrderProducts.get(orderProduct.nid);
                tmp.push(orderProduct);
                filteredOrderProducts.set(orderProduct.nid, tmp);
            } else {
                filteredOrderProducts.set(orderProduct.nid, [orderProduct]);
            }
        }
           
        return Object.fromEntries(filteredOrderProducts);
    }

    async getOrderProductData(orderId) {

        return await new Promise(function (resolve, reject) {
            orderProductModel.getOrderProduct(orderId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Order Product with id ${orderId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Order Product with id " + orderId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }
            });
        });
    }


    
}

module.exports = new OrderProductService;