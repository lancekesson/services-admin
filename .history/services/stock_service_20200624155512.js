stockModel = require('../models/stock_model');

class StockService {

    constructor() {
        
    }

    async getStockReceipt(supplierOrderRef) {

        return await new Promise(function (resolve, reject) {

            stockModel.getStockReceipt(supplierOrderRef, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Receipt  with id ${supplierOrderRef}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Stock Receipt with id " + supplierOrderRef);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }
            });
        });
    }
    
}

module.exports = new StockService;