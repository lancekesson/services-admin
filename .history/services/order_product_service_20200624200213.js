orderProductModel = require('../models/order_product_model');

class OrderProductService {

    constructor() {
        
    }

    async getOrderProduct(orderId) {

        const orderProducts = await this.getOrderProductData(orderId);
        const filteredOrderProducts= this.filterOrderProducts(orderProducts);

        return filteredOrderProducts;
        
    }

    filterOrderProducts(orderProducts) {

       // let newOrderProducts = orderProducts.filter(orderProduct => {
            let filteredOrderProducts = new Map();
            //orderProducts = JSON.stringify(orderProducts);
console.log(orderProducts)
            for(let orderProduct of orderProducts) {
                console.log('in');
                console.log(orderProduct)
                if(filteredOrderProducts.has(orderProduct.nid)) {

                } else {
                    filteredOrderProducts.set(orderProduct.nid, orderProduct);
                }
            }
           
     //   })

      //  console.log(filteredOrderProducts);
        return filteredOrderProducts;
    }

    async getOrderProductData(orderId) {

        return await new Promise(function (resolve, reject) {
            orderProductModel.getOrderProduct(orderId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Order Product with id ${orderId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Order Product with id " + orderId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }
            });
        });
    }


    
}

module.exports = new OrderProductService;