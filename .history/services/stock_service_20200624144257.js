stockModel = require('../models/stock_model');

class StockService {

    constructor() {
        
    }

    getStockReceipt(supplierOrderRef) {

        const stockReceipt = stockModel.getStockReceipt(supplierOrderRef)
        console.log(stockReceipt);
        return stockReceipt;
    }
}

module.exports = new StockService;