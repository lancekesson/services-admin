stockModel = require('../models/stock_model');

class StockService {

    constructor() {
        
    }

    getStockReceipts(supplierOrderRef) {
        return stockModel.getStockReceipts(supplierOrderRef);
    }
}

module.exports = new StockService;