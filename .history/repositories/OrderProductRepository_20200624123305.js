class OrderProductRepository {

    orderProductId;
    orderId;
    nid;
    title;
    manufacturer;
    model;
    qty;
    cost;
    price;
    weight;
    data;


    constructor(properties) {
        this.stockModel = properties;
    }

    getOrderProductId() {
        return this.orderProductId;
    }
}

module.exports = OrderProductRepository;