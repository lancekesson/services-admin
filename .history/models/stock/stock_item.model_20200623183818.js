const sql = require("../../includes/db.js");

// constructor
const StockItem = function () {
  //console.log('in constructor');
};


StockItem.findBy = (stockReceiptLineId, result) => {
  sql.query(`SELECT * FROM stock_items WHERE stock_receipt_line_id = '${stockReceiptLineId}'`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found stock_item: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

module.exports = StockItem;