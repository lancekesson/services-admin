const sql = require("../../includes/db.js");

// constructor
const StockItemUse = function() {
  //console.log('in constructor');
};


StockItemUse.findBy = (stockItemId, result) => {
  sql.query(`SELECT * FROM stock_item_uses WHERE stock_item_id = '${stockItemId}'`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found stock_item_use: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

module.exports = StockItemUse;