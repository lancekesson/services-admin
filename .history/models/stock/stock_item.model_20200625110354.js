const db = require("../../services/db_service");

class StockItem {

  constructor() {

  }

  findById = (stockReceiptLineId, result) => {
    db.query(`SELECT * FROM stock_items WHERE stock_receipt_line_id = '${stockReceiptLineId}'`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found stock_item: ", res[0]);
        repo = this.buildRepo(res);
        result(null, repo);
        return;
      }
  
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }

  buildRepo(res) {
  
    let ret = [];
    for (let i of res) {
        ret.push(Object.assign({}, i));
    }
  
    return ret;
  }

}

module.exports = new StockItem;