db = require('../../services/db.service');
class StockReceiptModel {


    constructor() {
       
    }

     getStockReceipt(supplierOrderRef, result) {
        db.query(`SELECT * FROM stock_receipts WHERE supplier_order_ref = '${supplierOrderRef}'`, (err, res) => {

          if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
          }
      
          if (res.length) {
            console.log("found stock_receipts: ", res[0]);
            let repo = this.buildRepo(res);
            result(null, repo);
            return;
          }
      
          // not found Customer with the id
          result({ kind: "not_found" }, null);
          return;
        });

    }

    createStockReceipt(stockReceiptDto) {

    }

    buildRepo(res) {

      let ret = [];
      for (let i of res) {
          ret.push(Object.assign({}, i));
      }

      return ret;
  }
}

module.exports = new StockReceiptModel;