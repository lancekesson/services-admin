const sql = require("../../includes/db.js");

// constructor
const StockReceiptLine = function() {
  //console.log('in constructor');
};


StockReceiptLine.findBy = (stockReceiptId, result) => {
  sql.query(`SELECT * FROM stock_receipt_lines WHERE stock_receipt_id = '${stockReceiptId}'`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found stock_receipt_line: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

module.exports = StockReceiptLine;