const db = require('../../../services/db.service');
const StockItem = require('../../entities/stock/StockItem.entity').default;
const StockItemRead = require('../../entities/stock/StockItemRead.entity');

class StockItemRespository {
  constructor() {

  }

  findById(stockReceiptLineId, result) {
    db.query(`SELECT * FROM stock_items WHERE stock_receipt_line_id = '${stockReceiptLineId}'`, (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log('found stock_item: ', res[0]);
        const repo = this.buildRepo(res);
        result(null, repo);
        return;
      }

      // not found Customer with the id
      result({ kind: 'not_found' }, null);
    });
  }

  create(stockItemDto, result) {
    if (stockItemDto instanceof StockItem) {
      db.query(
        'INSERT INTO stock_items SET ?', stockItemDto, (err, res) => {
          if (err) {
            console.log('error: ', err);
            result(err, null);
            return;
          }

          if (res.insertId) {
            console.log(`successfully added ${res.insertId}`);
            result(null, { stock_Item_id: res.insertId });
            return;
          }

          // not found Customer with the id
          result({ kind: 'not_found' }, null);
        },
      );
    } else {
      // not found Customer with the id
      result({ error: 'input is not an instance of StockItem' }, null);
    }
  }

  buildRepo(res) {
    const ret = [];
    for (const i of res) {
      const stockItemRead = new StockItemRead(i);
      ret.push(stockItemRead);
    }

    return ret;
  }
}

module.exports = new StockItemRespository();
