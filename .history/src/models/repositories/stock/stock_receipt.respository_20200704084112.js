const StockReceipt = require('../../entities/stock/StockReceipt.entity');
const StockReceiptRead = require('../../entities/stock/StockReceiptRead.entity');

db = require('../../../services/db.service');
class StockReceiptRespository {


    constructor() {
       
    }

     getStockReceipt(supplierOrderRef, result) {
        db.query(`SELECT * FROM stock_receipts WHERE supplier_order_ref = '${supplierOrderRef}'`, (err, res) => {

          if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
          }
      
          if (res.length) {
            console.log("found stock_receipts: ", res[0]);
            let repo = this.buildRepo(res);
            result(null, repo);
            return;
          }
      
          // not found Customer with the id
          result({ kind: "not_found" }, null);
          return;
        });

    }

    create(stockReceiptDto, result) {
    
      if(stockReceiptDto instanceof StockReceipt) {
        db.query(
          'INSERT INTO stock_receipts SET ?', stockReceiptDto, (err, res) => {
          if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
          }
    
          if (res.insertId) {
            console.log("successfully added " + res.insertId);
            result(null, { stock_receipt_line_id: res.insertId });
            return;
          }
      
          // not found Customer with the id
          result({ kind: "not_found" }, null);
        });
      } else {
        // not found Customer with the id
        result({ error: "input is not an instance of StockReceipt" }, null);
      }
      
    }

    buildRepo(res) {

      let ret = [];
      for (let i of res) {
        let stockReceiptRead = new StockReceiptRead(i);
        ret.push(stockReceiptRead);
      }

      return ret;
  }
}

module.exports = new StockReceiptRespository;