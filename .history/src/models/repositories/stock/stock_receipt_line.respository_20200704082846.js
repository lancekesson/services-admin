const db = require('../../../services/db.service');
const StockReceiptLine = require('../../entities/stock/StockReceiptLine.entity');

class StockReceiptLineRespository {

  constructor() {

  }

  findById(stockReceiptId, result) {
    db.query(`SELECT * FROM stock_receipt_lines WHERE stock_receipt_id = '${stockReceiptId}'`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found stock_receipt_line: ", res[0]);
        let repo = this.buildRepo(res);
        result(null, repo);
        return;
      }
  
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  };

  create(stockReceiptLineDto, result) {
    
    if(stockReceiptLineDto instanceof StockReceiptLine) {
      db.query(
        'INSERT INTO stock_receipt_lines SET ?', stockReceiptLineDto, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }
  
        if (res.insertId) {
          console.log("successfully added " + res.insertId);
          result(null, { stock_receipt_line_id: res.insertId });
          return;
        }
    
        // not found Customer with the id
        result({ kind: "not_found" }, null);
      });
    } else {
      // not found Customer with the id
      result({ error: "input is not an instance of StockReceiptLine" }, null);
    }
    
  }

  
  buildRepo(res) {
  
    let ret = [];
    for (let i of res) {
        ret.push(Object.assign({}, i));
    }
  
    return ret;
  }

}


module.exports = new StockReceiptLineRespository;