class AtdReturnLine {

    constructor(properties) {
        console.log(properties);
        if(this.validateInput(properties)){
            return this;
        } else {
            return false;
        }
    }

    validateInput(properties) {

        const notValid = false;

        if(properties.hasOwnProperty('order_id')) {
            this.order_id = properties.order_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('return_order_product_id')) {
            this.return_order_product_id = properties.return_order_product_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('original_order_product_id')) {
            this.original_order_product_id = properties.original_order_product_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('return_qty')) {
            this.return_qty = properties.return_qty;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('status')) {
            this.status = properties.status;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('received_stock_line_id')) {
            this.received_stock_line_id = properties.received_stock_line_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('confirmed_stock_line_id')) {
            this.confirmed_stock_line_id = properties.confirmed_stock_line_id;
        } else {
            return notValid;
        }

    }
}

module.exports = AtdReturnLine;