class StockItemReadModel {

    constructor(properties) {
        console.log(properties);
        if(this.validateInput(properties)){
            return this;
        } else {
            return false;
        }
    }

    validateInput(properties) {

        const notValid = false;

        if(properties.hasOwnProperty('stock_item_id')) {
            this.stockReceiptLineId = properties.stock_item_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('stock_receipt_line_id')) {
            this.stockReceiptLineId = properties.stock_receipt_line_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('product_nid')) {
            this.productNid = properties.product_nid;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('location_id')) {
            this.locationId = properties.location_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('raw')) {
            this.raw = properties.raw;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('serial')) {
            this.serial = properties.serial;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('quantity')) {
            this.quantity = properties.quantity;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('allocated')) {
            this.allocated = properties.allocated;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('status_id')) {
            this.statusId = properties.status_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('percentage_refunded')) {
            this.percentageRefunded = properties.percentage_refunded;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('type')) {
            this.type = properties.type;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('created')) {
            this.created = properties.created;
        } else {
            return notValid;
        }
        
        return this;

    }

}

module.exports = StockItemReadModel;