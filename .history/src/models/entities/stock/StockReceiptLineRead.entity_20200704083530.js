class StockReceiptLineReadEntity {

    constructor(properties) {
        console.log(properties);
        if(this.validateInput(properties)){
            return this;
        } else {
            return false;
        }
    }

    validateInput(properties) {

        const notValid = false;

        if(properties.hasOwnProperty('stock_receipt_id')) {
            this.stockReceiptId = properties.stock_receipt_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('supplier_sku')) {
            this.supplierSku = properties.supplier_sku;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('line_sku')) {
            this.lineSku = properties.line_sku;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('product_nid')) {
            this.productNid = properties.product_nid;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('ticket_name')) {
            this.ticketName = properties.ticket_name;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('supplier_name')) {
            this.supplierName = properties.supplier_name;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('line_qty')) {
            this.lineQty = properties.line_qty;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('line_qty_deleted')) {
            this.lineQtyDeleted = properties.line_qty_deleted;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('currency')) {
            this.currency = properties.currency;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('exchange_rate')) {
            this.exchangeRate = properties.exchange_rate;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('unit_cost')) {
            this.unitCost = properties.unit_cost;
        } else {
            return notValid;
        }	

        if(properties.hasOwnProperty('unit_cost_bc')) {
            this.unitCostBc = properties.unit_cost_bc;
        } else {
            return notValid;
        }	

        if(properties.hasOwnProperty('atd_return_lines_received')) {
            this.atdReturnLinesReceived = properties.atd_return_lines_received;
        } else {
            return notValid;
        }	

        if(properties.hasOwnProperty('atd_return_lines_confirmed')) {
            this.atdReturnLinesConfirmed = properties.atd_return_lines_confirmed;
        } else {
            return notValid;
        }
        
        return this;

    }

}

module.exports = StockReceiptLineReadEntity;