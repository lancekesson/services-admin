const CreatedDate = require('../../classes/CreatedDate');

class StockItemEntity {

    constructor(properties) {
        console.log(properties);
        if (this.validateInput(properties)) {
            return this;
        } else {
            return false;
        }
    }

    validateInput(properties) {

        const notValid = false;

        if (properties.hasOwnProperty('stock_receipt_line_id')) {
            this.stock_receipt_line_id = properties.stock_receipt_line_id;
        } else {
            return notValid;
        }

        if (properties.hasOwnProperty('product_nid')) {
            this.product_nid = properties.product_nid;
        } else {
            return notValid;
        }

        if (properties.hasOwnProperty('location_id')) {
            this.location_id = properties.location_id;
        } else {
            return notValid;
        }

        if (properties.hasOwnProperty('raw')) {
            this.raw = properties.raw;
        } else {
            return notValid;
        }

        if (properties.hasOwnProperty('serial')) {
            this.serial = properties.serial;
        } else {
            return notValid;
        }

        if (properties.hasOwnProperty('quantity')) {
            this.quantity = properties.quantity;
        } else {
            return notValid;
        }

        if (properties.hasOwnProperty('allocated')) {
            this.allocated = properties.allocated;
        } else {
            return notValid;
        }

        if (properties.hasOwnProperty('status_id')) {
            this.status_id = properties.status_id;
        } else {
            return notValid;
        }

        if (properties.hasOwnProperty('percentage_refunded')) {
            this.percentage_refunded = properties.percentage_refunded;
        } else {
            return notValid;
        }

        if (properties.hasOwnProperty('type')) {
            this.type = properties.type;
        } else {
            return notValid;
        }

        this.created = new CreatedDate().generateCreateDate();

        return this;

    }

}

module.exports = StockItemEntity;