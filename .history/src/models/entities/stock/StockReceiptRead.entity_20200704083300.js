class StockReceiptReadEntity {

    constructor(properties) {
        console.log(properties);
        if(this.validateInput(properties)){
            return this;
        } else {
            return false;
        }
    }

    validateInput(properties) {

        const notValid = false;

        if(properties.hasOwnProperty('user_id')) {
            this.userId = properties.user_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('receipt_type')) {
            this.receiptType = properties.receipt_type;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('receipt_date')) {
            this.receiptDate = properties.receipt_date;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('location_id')) {
            this.locationId = properties.location_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('order_ref')) {
            this.orderRef = properties.order_ref;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('stock_order_id')) {
            this.stockOrderId = properties.stock_order_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('order_id')) {
            this.orderId = properties.order_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('supplier_order_ref')) {
            this.supplierOrderRef = properties.supplier_order_ref;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('supplier_payment_date')) {
            this.supplierPaymentDate = properties.supplier_payment_date;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('purchase_order_ref')) {
            this.purchaseOrderRef = properties.purchase_order_ref;
        } else {
            return notValid;
        }	

        return this;

    }

}

module.exports = StockReceiptReadEntity;