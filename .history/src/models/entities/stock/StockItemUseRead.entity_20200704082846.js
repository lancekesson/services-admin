const CreatedDate = require("../../classes/CreatedDate");

class StockItemUseReadEntity {

    constructor(properties) {
        console.log(properties);
        if(this.validateInput(properties)){
            return this;
        } else {
            return false;
        }
    }

    validateInput(properties) {

        const notValid = false;

        if(properties.hasOwnProperty('stock_item_id')) {
            this.stock_item_id = properties.stock_item_id;
        } else {
            return notValid;
        }

        if(properties.order_product_id) {
            this.order_product_id = properties.order_product_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('related_stock_item_id')) {
            this.related_stock_item_id = properties.related_stock_item_id;
        } else {
            return notValid;
        }

        if(properties.quantity) {
            this.quantity = properties.quantity;
        } else {
            return notValid;
        }

        this.created = new CreatedDate().generateCreateDate();
        
        return this;

    }
}

module.exports = StockItemUseReadEntity;