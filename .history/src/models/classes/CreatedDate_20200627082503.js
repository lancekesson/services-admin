class CreatedDate {

    generateCreateDate() {
        const nowDate = new Date();
        return this.formatCreateDate(nowDate);
    }

    formatCreateDate(date) {
        return date.toISOString().slice(0, 19).replace('T', ' ');
    }

}