const db = require("../../services/db.service");
const StockItemUse = require("../../repositories/classes/StockItemUse");

class AtdReturnLinesModel {

  constructor() {

  }

  findById(orderId, result) {
    db.query(`SELECT * FROM atd_return_lines WHERE order_id = '${orderId}'`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found atd_return_lines: ", res[0]);
        let repo = this.buildRepo(res);
        result(null, repo);
        return;
      }
  
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }

  create(atdReturnLineEntity, result) {
    
    if(atdReturnLineEntity instanceof AtdReturnLine) {
      db.query(
        'INSERT INTO atd_return_lines SET ?', atdReturnLineEntity, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }
  
        if (res.insertId) {
          console.log("successfully added " + res.insertId);
          result(null, { atd_return_line_id: res.insertId });
          return;
        }
    
        // not found Customer with the id
        result({ kind: "not_found" }, null);
      });
    } else {
      // not found Customer with the id
      result({ error: "input is not an instance of AtdReturnLine" }, null);
    }
    
  }

  buildRepo(res) {
  
    let ret = [];
    for (let i of res) {
        ret.push(Object.assign({}, i));
    }
  
    return ret;
  }

}

module.exports = new AtdReturnLinesModel;