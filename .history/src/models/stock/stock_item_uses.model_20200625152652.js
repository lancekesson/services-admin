const db = require("../../services/db.service");

class StockItemUse {

  constructor() {

  }

  findById(stockItemId, result) {
    db.query(`SELECT * FROM stock_item_uses WHERE stock_item_id = '${stockItemId}'`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found stock_item_use: ", res[0]);
        let repo = this.buildRepo(res);
        result(null, repo);
        return;
      }
  
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }

  create(stockItemDto, result) {

    db.query(
      'INSERT INTO stock_item_uses SET ?', stockItemDto, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.insertId) {
        console.log("successfully added " + res.insertId);
        result(null, res.insertId);
        return;
      }
  
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }

  buildRepo(res) {
  
    let ret = [];
    for (let i of res) {
        ret.push(Object.assign({}, i));
    }
  
    return ret;
  }

}

module.exports = new StockItemUse;