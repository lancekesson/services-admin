
class OrderProductModel {

    constructor() {
       
    }

    getOrderProduct(orderId, result) {
        db.query(`SELECT * FROM uc_order_products WHERE order_id = '${orderId}'`, (err, res) => {

            if (err) {
              console.log("error: ", err);
              result(err, null);
              return;
            }
        
            if (res.length) {
              console.log("found uc_order_products: ", res[0]);
              let repo = this.buildOrderProductRepo(res);
              result(null, repo);
              return;
            }
        
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        });

    }

    createOrderProduct(orderProductDto) {

    }

    buildOrderProductRepo(res) {

        let ret = [];
        for (let i of res) {
            ret.push(Object.assign({}, i));
        }

        return ret;
    }
}

module.exports = new OrderProductModel;