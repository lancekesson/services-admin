const fs = require('fs')

const fileService = {

  // helper methods
  readFile: (filePath, encoding = 'utf8') => {
    const file = fs.readFileSync(filePath, encoding)
    return file
  },

  writeFile: function (fileData, filePath = '', encoding = 'utf8') {
    fs.writeFileSync(filePath, fileData)
  },

  writeDirAndFile: function (fileData, fileDir = '', fileName = '') {
    const filePath = fileDir + '/' + fileName

    if (fs.existsSync(fileDir)) {
      this.writeFile(fileData, filePath)
    } else {
      fs.mkdirSync(fileDir)
      this.writeFile(fileData, filePath)
    }
  }
}

module.exports = fileService
