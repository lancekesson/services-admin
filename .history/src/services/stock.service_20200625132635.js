const StockReceiptModel = require("../models/stock/stock_receipt.model");
const StockReceiptLineModel = require('../models/stock/stock_receipt_line.model');
const StockItemModel = require('../models/stock/stock_item.model');
const StockItemUseModel = require('../models/stock/stock_item_uses.model');
const StockItemUse = require('../repositories/classes/StockItemUse');

// constructor
class StockService {
    //console.log('in constructor');
    constructor() {

    }

    async getStockReceipt(supplierOrderRef) {

        return await new Promise(function (resolve, reject) {

            StockReceiptModel.getStockReceipt(supplierOrderRef, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Customer with id ${supplierOrderRef}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Customer with id " + supplierOrderRef);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async getStockReceiptLines(stockReceiptId) {
        return await new Promise(function (resolve, reject) {

            StockReceiptLineModel.findById(stockReceiptId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Receipt with id ${stockReceiptId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Stock Receipt with id " + stockReceiptId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async getStockItems(stockReceiptLineId) {
        return await new Promise(function (resolve, reject) {

            StockItemModel.findById(stockReceiptLineId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Item with id ${stockReceiptLineId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Stock Item with id " + stockReceiptLineId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async getStockItemUse(stockItemId) {
        return await new Promise(function (resolve, reject) {

            StockItemUseModel.findById(stockItemId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Item Use with id ${stockItemId}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Stock Item Use with id " + stockItemId);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async addStockItemUse(stockItemId) {
        return await new Promise(function (resolve, reject) {

            StockItemUseModel.create(stockItemUseDto, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Item Use with id ${stockItemId}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Stock Item Use with id " + stockItemId);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    buildStockItemUse(params) {
        console.log('126');
        console.log(params);
        return new StockItemUse(params);
    }

}

module.exports = new StockService;