const StockService = require('../services/stock.service');

// constructor
class StockStatusService {
    //console.log('in constructor');
    constructor() {

    }

    getStockStatus(orderId) {
        let stockCheck ;
        let atdReturnLines = StockService.getReturnLine(orderId);
console.log(atdReturnLines)
        stockCheck = {'atdReturnLines' : atdReturnLines};

        return stockCheck;
    }

}

module.exports = new StockStatusService;