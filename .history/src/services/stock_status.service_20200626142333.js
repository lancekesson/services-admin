const StockService = require('../services/stock.service');

// constructor
class StockStatusService {
    //console.log('in constructor');
    constructor() {

    }

    getStockStatus(orderId) {
        let stockCheck = {};
        stockCheck.atdReturnLines = StockService.getReturnLine(orderId);

        return stockCheck.atdReturnLines;
    }

}

module.exports = new StockStatusService;