const StockService = require('../services/stock.service')

// constructor
class StockStatusService {
    //console.log('in constructor');
  constructor() {

  }

  async getStockStatus(orderId) {
    let stockCheck = {};
    stockCheck.atdReturnLines = await StockService.getReturnLine(orderId);
  //  stockCheck = {'atdReturnLines' : atdReturnLines};
    //stockCheck.stockReceipts = await StockService.getStockReceipt();
    //stockCheck.stockReceiptLines = await StockService.getStockReceiptLines();
    // stockCheck.stockItems = await StockService.getStockItems();
    // stockCheck.stockItemLines = await StockService.getStockItemUse();
    return stockCheck
  }
}

module.exports = new StockStatusService()
