let readModUpdateService = {

    updateModData: function (readData) {
        return this.extractReadData(JSON.parse(readData));
    },

    extractReadData: function (readData) {

        let extractedRoomStay  = [];
        let modifiedReadData = {};
        let tempRoomStays = {};

        let roomStays = readData._RESPONSE.roomStays.roomStays;

        for( let roomStay of roomStays) {
            extractedRoomStay.push(roomStay.roomStay);
            
        }
 
        tempRoomStays.roomStay = extractedRoomStay;

        modifiedReadData.roomStays = tempRoomStays;
        modifiedReadData.resGuests = readData._RESPONSE.resGuests;
        modifiedReadData.resGlobalInfo = readData._RESPONSE.resGlobalInfo;

        return modifiedReadData;

    },

    updateResGuests: function (resGuests) {

        for (let resGuest of resGuests) {
            const givenName = resGuest.profiles.profileInfo.profile.customer.personName.givenName;

            if(givenName == 'Mr' || givenName == 'Mrs' || givenName == 'Miss' || givenName == 'Ms' || givenName == 'Dr') {
                const surname = resGuest.profiles.profileInfo.profile.customer.personName.surname;
                let names = surname.split(' ');
                resGuest.profiles.profileInfo.profile.customer.personName.givenName = names[0];
                resGuest.profiles.profileInfo.profile.customer.personName.surname = names[1];
            }

        }
        return resGuests
    },

    updateRoomStays: function (data) {

        let resGuestsCount = data.resGuests.length;
        let roomStaysCount = data.roomStays.roomStay.length;

        for (let i = 0; i < roomStaysCount; i++) {

            let guestCountCount = parseInt(data.roomStays.roomStay[i].guestCounts.length);

            if(resGuestsCount > guestCountCount) {
             
                for (let guestCount of data.roomStays.roomStay[i].guestCounts) {

                    let resGuestRphs = [];

                    let guestCountQty = parseInt(guestCount.guestCount.count);
                    resGuestsCount = resGuestsCount - guestCountQty;

                    while (guestCountQty !== 0) {
                        let resGuestRphTemp = {};
                        resGuestRphTemp.resGuestRph = { 'rph' : guestCountQty--};

                        resGuestRphs.unshift(resGuestRphTemp);
                    }

                    data.roomStays.roomStay[i].resGuestRphs = resGuestRphs;

                }
            }
        }

        return data.roomStays;
    }

};

module.exports = readModUpdateService;