const readModUpdateService = {

  commandModData: function (jsonData) {
    const modData = {}
    const modReadData = this.updateModData(jsonData)

    modData.roomStays = this.updateRoomStays(modReadData)
    modData.resGuests = this.updateResGuests(modReadData.resGuests)
    modData.resGlobalInfo = modReadData.resGlobalInfo

    return modData
  },

  updateModData: function (readData) {
    return this.extractReadData(JSON.parse(readData))
  },

  extractReadData: function (readData) {
    const extractedRoomStay = []
    const modifiedReadData = {}
    const tempRoomStays = {}

    const roomStays = readData._RESPONSE.roomStays.roomStays

    for (const roomStay of roomStays) {
      extractedRoomStay.push(roomStay.roomStay)
    }

    tempRoomStays.roomStay = extractedRoomStay

    modifiedReadData.roomStays = tempRoomStays
    modifiedReadData.resGuests = readData._RESPONSE.resGuests
    modifiedReadData.resGlobalInfo = readData._RESPONSE.resGlobalInfo

    return modifiedReadData
  },

  updateResGuests: function (resGuests) {
    for (const resGuest of resGuests) {
      const givenName = resGuest.profiles.profileInfo.profile.customer.personName.givenName

      if (givenName === 'Mr' || givenName === 'Mrs' || givenName === 'Miss' || givenName === 'Ms' || givenName === 'Dr') {
        const surname = resGuest.profiles.profileInfo.profile.customer.personName.surname
        const names = surname.split(' ')
        resGuest.profiles.profileInfo.profile.customer.personName.givenName = names[0]
        resGuest.profiles.profileInfo.profile.customer.personName.surname = names[1]
      }
    }

    return resGuests
  },

  updateRoomStays: function (data) {
    let resGuestsCount = data.resGuests.length
    const roomStaysCount = data.roomStays.roomStay.length
    const resGuestRphs = []

    for (let i = 0; i < roomStaysCount; i++) {
      const guestCountCount = parseInt(data.roomStays.roomStay[i].guestCounts.length)

      if (resGuestsCount > guestCountCount) {
        for (const guestCount of data.roomStays.roomStay[i].guestCounts) {

          let guestCountQty = parseInt(guestCount.guestCount.count)
          resGuestsCount = resGuestsCount - guestCountQty

          while (guestCountQty !== 0) {
            const resGuestRphTemp = {}

            let resGuestRphsLength = resGuestRphs.length

            resGuestRphTemp.resGuestRph = { rph: ++resGuestRphsLength }

            resGuestRphs.unshift(resGuestRphTemp)
            guestCountQty--
          }

          data.roomStays.roomStay[i].resGuestRphs = resGuestRphs
        }
      }
    }

    return data.roomStays
  }

}

module.exports = readModUpdateService
