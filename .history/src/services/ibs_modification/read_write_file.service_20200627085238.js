const fs = require('fs');

let readWriteFileService = {

    // helper methods
    readFile: (filePath , encoding = 'utf8') => {
        const file = fs.readFileSync(filePath, encoding);
        return file;
    },

    writeFile : function (fileData, filePath = '', encoding = 'utf8') {

        fs.writeFileSync(filePath, fileData);
    },

    writeDirAndFile: function (fileData, fileDir = '', fileName = '') {

        let dataFileDir = 'data/' + fileDir;
        let filePath = dataFileDir + '/' + fileName;

        if(fs.existsSync(dataFileDir)) {
            this.writeFile(fileData, filePath);
        } else {
            fs.mkdirSync(dataFileDir);
            this.writeFile(fileData, filePath);
        }
    }
    
};

module.exports = readWriteFileService;