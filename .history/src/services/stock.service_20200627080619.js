const StockReceiptModel = require("../models/stock/stock_receipt.model");
const StockReceiptLineModel = require('../models/stock/stock_receipt_line.model');
const StockItemModel = require('../models/stock/stock_item.model');
const StockItemUseModel = require('../models/stock/stock_item_uses.model');
const StockItemUse = require('../models/entities/StockItemUse');
const AtdReturnLine = require("../models/entities/AtdReturnLine");
const AtdReturnLinesModel = require('../models/atd_return_lines.model');
const StockReceipt = require("../models/entities/StockReceipt");
const StockReceiptLine = require("../models/entities/StockReceiptLine");
const StockItem = require("../models/entities/StockItem");

// constructor
class StockService {
    //console.log('in constructor');
    constructor() {

    }

    async getStockReceipt(supplierOrderRef) {

        return await new Promise(function (resolve, reject) {

            StockReceiptModel.getStockReceipt(supplierOrderRef, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Receipt  with id ${supplierOrderRef}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Stock Receipt  with id " + supplierOrderRef);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async addStockReceipt(stockReceiptDto) {
        return await new Promise(function (resolve, reject) {

            StockReceiptModel.create(stockReceiptDto, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Receipt with id ${stockReceiptDto}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Stock Receipt with id " + stockReceiptDto);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    buildStockReceipt(params) {
        return  new StockReceipt(params);
    }

    async getStockReceiptLines(stockReceiptId) {
        return await new Promise(function (resolve, reject) {

            StockReceiptLineModel.findById(stockReceiptId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Receipt Line with id ${stockReceiptId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Stock Receipt Line with id " + stockReceiptId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async addStockReceiptLine(stockReceiptLineDto) {
        return await new Promise(function (resolve, reject) {

            StockReceiptLineModel.create(stockReceiptLineDto, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Receipt Line with id ${stockReceiptLineDto}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Stock Receipt Line with id " + stockReceiptLineDto);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    buildStockReceiptLine(params) {
        return  new StockReceiptLine(params);
    }

    async getStockItems(stockReceiptLineId) {
        return await new Promise(function (resolve, reject) {

            StockItemModel.findById(stockReceiptLineId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Item with id ${stockReceiptLineId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Stock Item with id " + stockReceiptLineId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async addStockItem(stockItemDto) {
        return await new Promise(function (resolve, reject) {

            StockItemModel.create(stockItemDto, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Item with id ${stockItemDto}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Stock Item with id " + stockItemDto);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    buildStockItem(params) {
        return  new StockItem(params);
    }

    async getStockItemUse(stockItemId) {
        return await new Promise(function (resolve, reject) {

            StockItemUseModel.findById(stockItemId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Item Use with id ${stockItemId}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Stock Item Use with id " + stockItemId);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async addStockItemUse(stockItemUseDto) {
        return await new Promise(function (resolve, reject) {

            StockItemUseModel.create(stockItemUseDto, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Stock Item Use with id ${stockItemUseDto}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Stock Item Use with id " + stockItemUseDto);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    buildStockItemUse(params) {
        return  new StockItemUse(params);
    }

    async getReturnLine(orderId) {
        return await new Promise(function (resolve, reject) {

            AtdReturnLinesModel.findById(orderId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Atd Return Line with id ${orderId}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Atd Return Line with id " + orderId);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    async addReturnLine(returnLine) {
        return await new Promise(function (resolve, reject) {

            AtdReturnLinesModel.create(returnLine, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Atd Return Line with id ${returnLine}.`);
                        resolve(err);
                    } else {
                        console.log("Error retrieving Atd Return Line with id " + returnLine);
                        resolve(err);
                    }
                } else {
                    resolve(data);
                }


            });
        });
    }

    buildReturnLine(params) {
        return  new AtdReturnLine(params);
    }

}

module.exports = new StockService;