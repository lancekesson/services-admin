const StockService = require('../services/stock.service');

// constructor
class StockStatusService {
    //console.log('in constructor');
    constructor() {

    }

    async getStockStatus(orderId) {
        let stockCheck = {};
        stockCheck.atdReturnLines = await StockService.getReturnLine(orderId);
      //  stockCheck = {'atdReturnLines' : atdReturnLines};

        return stockCheck;
    }

}

module.exports = new StockStatusService;