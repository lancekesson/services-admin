class StockItemUse {

    constructor(properties) {
        if(this.validateInput(properties)){
            return this;
        } else {
            return false;
        }
    }

    validateInput(properties) {

        const notValid = false;

        if(properties.stock_item_id) {
            this.stock_item_use_id = properties.stock_item_id;
        } else {
            return notValid;
        }

        if(properties.order_product_id) {
            this.order_product_id = properties.order_product_id;
        } else {
            return notValid;
        }

        if(properties.related_stock_item_id) {
            this.related_stock_item_id = properties.related_stock_item_id;
        } else {
            return notValid;
        }

        if(properties.quantity) {
            this.quantity = properties.quantity;
        } else {
            return notValid;
        }

        this.created = new Date().toISOString;

        return this;

    }

}

module.exports = new StockItemUse;