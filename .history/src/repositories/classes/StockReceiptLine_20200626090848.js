class StockReceiptLine {

    constructor(properties) {
        console.log(properties);
        if(this.validateInput(properties)){
            return this;
        } else {
            return false;
        }
    }

    validateInput(properties) {

        const notValid = false;

        if(properties.hasOwnProperty('stock_receipt_id')) {
            this.stock_receipt_id = properties.stock_receipt_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('supplier_sku')) {
            this.supplier_sku = properties.supplier_sku;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('line_sku')) {
            this.line_sku = properties.line_sku;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('product_nid')) {
            this.product_nid = properties.product_nid;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('ticket_name')) {
            this.ticket_name = properties.ticket_name;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('supplier_name')) {
            this.supplier_name = properties.supplier_name;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('line_qty')) {
            this.line_qty = properties.line_qty;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('line_qty_deleted')) {
            this.line_qty_deleted = properties.line_qty_deleted;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('currency')) {
            this.currency = properties.currency;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('exchange_rate')) {
            this.exchange_rate = properties.exchange_rate;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('unit_cost')) {
            this.unit_cost = properties.unit_cost;
        } else {
            return notValid;
        }	

        if(properties.hasOwnProperty('unit_cost_bc')) {
            this.unit_cost_bc = properties.unit_cost_bc;
        } else {
            return notValid;
        }	

        if(properties.hasOwnProperty('atd_return_lines_received')) {
            this.atd_return_lines_received = properties.atd_return_lines_received;
        } else {
            return notValid;
        }	

        if(properties.hasOwnProperty('atd_return_lines_confirmed')) {
            this.atd_return_lines_confirmed = properties.atd_return_lines_confirmed;
        } else {
            return notValid;
        }
        
        return this;

    }

}

module.exports = StockReceiptLine;