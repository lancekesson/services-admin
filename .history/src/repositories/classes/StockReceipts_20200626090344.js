class StockReceipt {

    constructor(properties) {
        console.log(properties);
        if(this.validateInput(properties)){
            return this;
        } else {
            return false;
        }
    }

    validateInput(properties) {

        const notValid = false;

        if(properties.hasOwnProperty('user_id')) {
            this.user_id = properties.user_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('receipt_type')) {
            this.receipt_type = properties.receipt_type;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('receipt_date')) {
            this.receipt_date = properties.receipt_date;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('location_id')) {
            this.location_id = properties.location_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('order_ref')) {
            this.order_ref = properties.order_ref;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('stock_order_id')) {
            this.stock_order_id = properties.stock_order_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('order_id')) {
            this.allocorder_idated = properties.order_id;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('supplier_order_ref')) {
            this.supplier_order_ref = properties.supplier_order_ref;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('supplier_payment_date')) {
            this.supplier_payment_date = properties.supplier_payment_date;
        } else {
            return notValid;
        }

        if(properties.hasOwnProperty('purchase_order_ref')) {
            this.purchase_order_ref = properties.purchase_order_ref;
        } else {
            return notValid;
        }	

       
        this.created = this.generateCreateDate();
        
        return this;

    }


    generateCreateDate() {
        const nowDate = new Date();
        return this.formatCreateDate(nowDate);
    }

    formatCreateDate(date) {
        return date.toISOString().slice(0, 19).replace('T', ' ');
    }

}

module.exports = StockReceipt;