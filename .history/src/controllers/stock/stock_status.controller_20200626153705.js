stockStatusService = require('../../services/stock_status.service');
orderProductService = require('../../services/order_product.service');

class StockStatusController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }


    async checkStock(req, res, next) {
      const confId = req.params.confId;
      let orderProduct = await orderProductService.getOrderProductByConf(confId);

     // let stockStatus = await stockStatusService.getStockStatus(orderId);

      let data = {
        "order_product" : orderProduct,
        "stock_status": stockStatus
      };

      res.send(data);

    }

}

module.exports = new StockStatusController;