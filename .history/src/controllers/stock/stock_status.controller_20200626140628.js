stockCheckService = require('../../services/stock_check.service');
orderProductService = require('../../services/order_product.service');

class StockStatusController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }


    async checkStock(req, res, next) {
      const orderId = req.params.orderId;
      let orderProduct = await orderProductService.getOrderProduct(orderId);

      let stockStatus = await stockCheckService.getStockStatus(orderId);


      res.send(orderProduct);

    }

}

module.exports = new StockStatusController;