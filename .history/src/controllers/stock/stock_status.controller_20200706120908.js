const stockStatusService = require('../../services/stock_status.service')
const orderProductService = require('../../services/order_product.service')

class StockStatusController {
  constructor (stockModel) {
    this.stockModel = stockModel
  }

  async checkStock (req, res, next) {
    const confId = req.params.confId
    const stockStatus = []

    const orderProducts = await orderProductService.getOrderProductData(confId)

    for (const orderProduct of orderProducts) {
      console.log(orderProduct)
      stockStatus[stockStatus.length] = await stockStatusService.getStockStatus(orderProduct.order_id)
    }

    let data = {
      "order_product": orderProducts,
      "stock_status": stockStatus
    };

    res.send(data);

  }

}

module.exports = new StockStatusController;