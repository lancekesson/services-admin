stockService = require('../services/stock.service');
orderProductService = require('../services/order_product.service');

class StockReceiptLineController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }

    async getStockReceiptLines(req, res, next) {
      const stockReceiptId = req.params.stockReceiptId;

      let stockReceiptLines = await stockService.getStockReceiptLines(stockReceiptId);
      res.send(stockReceiptLines);

    }

    async addStockReceiptLine(req, res, next) {

      const stockReceiptLineDto = stockService.buildStockReceiptLine(req.body);
      let stockReceiptLine = await stockService.addStockReceiptLine(stockReceiptLineDto);
      res.send(stockReceiptLine);

    }

}

module.exports = new StockReceiptLineController;