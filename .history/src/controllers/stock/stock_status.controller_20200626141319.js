stockCheckService = require('../../services/stock_check.service');
orderProductService = require('../../services/order_product.service');

class StockStatusController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }


    async checkStock(req, res, next) {
      const orderId = req.params.orderId;
      let orderProduct = await orderProductService.getOrderProduct(orderId);

      let stockStatus = await stockCheckService.getStockStatus(orderId);

      data = {
        "order_product" : orderProduct,
        "stock_status": stockStatus
      };

      res.send(data);

    }

}

module.exports = new StockStatusController;