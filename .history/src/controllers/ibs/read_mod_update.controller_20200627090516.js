readWriteFile = require('../../services/ibs_modification/read_write_file.service.js');
readModUpdate = require('../../services/ibs_modification/read_mod_update.service.js');
const path = require('path');
let Request = require("request");

class IbsModificationController {

    doReadWriteNameMod(req, res, next) {

        // variables
        let collectedModData = [];

        let files = fs.readdirSync('tmp/');
        let modData = {};

        for( file of files) {

            let dataPath = 'tmp/' + file;

            let jsonData = readWriteFile.readFile(
                dataPath
                );

            let fileDirName = path.parse(file).name;
    
            let initialReadFile = fileDirName + '_read.json'
    
            readWriteFile.writeDirAndFile(jsonData, fileDirName, initialReadFile)
    
            modReadData = readModUpdate.updateModData(jsonData);
    
            modData.roomStays = readModUpdate.updateRoomStays(modReadData);
            modData.resGuests = readModUpdate.updateResGuests(modReadData.resGuests);
            modData.resGlobalInfo = modReadData.resGlobalInfo;

            let initialModFile = fileDirName + '_mod.json';
            readWriteFile.writeDirAndFile(JSON.stringify(modData), fileDirName, initialModFile)

            collectedModData.push(modData);
        }
        

        res.send(collectedModData);
    }

    doApiReadWriteNameMod(req, res, next) {

        // variables
        let collectedModData = [];

        let orderId = req.query.order_id;
        let modData = {};
        let collectedModData = [];
        let jsonData = {};

        let dataPath = 'data/';
        let fileDirName = orderId;

        let initialReadFile = orderId + '_read.json';

        Request.get("http://localhost:8099/gateway/read/:orderId", (error, response, body) => {
            if(error) {
                return console.log(error);
            }
            jsonData = JSON.parse(body);

            readWriteFile.writeDirAndFile(jsonData, fileDirName, initialReadFile)
    
            modReadData = readModUpdate.updateModData(jsonData);
        
            modData.roomStays = readModUpdate.updateRoomStays(modReadData);
            modData.resGuests = readModUpdate.updateResGuests(modReadData.resGuests);
            modData.resGlobalInfo = modReadData.resGlobalInfo;

            let initialModFile = orderId + '_mod.json';
            readWriteFile.writeDirAndFile(JSON.stringify(modData), fileDirName, initialModFile);

            //send data to api

            collectedModData.push(modData);
            
            res.send(collectedModData);
        });
        
    }

}

module.exports = new IbsModificationController;