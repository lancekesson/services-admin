const readWriteFile = require('../../services/file.service.js')
const readModUpdate = require('../../services/ibs_modification/read_mod_update.service.js')
const path = require('path')
const Request = require('request')
const fs = require('fs')

class IbsModificationController {
  doReadWriteNameMod (req, res, next) {
    // variables
    const collectedModData = []
    const files = fs.readdirSync('/Users/lancekesson/Projects/services-admin/data_import/')
    // let modData = {};

    for (const file of files) {
      const dataPath = '/Users/lancekesson/Projects/services-admin/data_import/' + file

      const jsonData = readWriteFile.readFile(dataPath)

      const fileDirName = path.parse(file).name

      const initialReadFile = fileDirName + '_read.json'

      const dataFileDir = '/Users/lancekesson/Projects/services-admin/data_output/' + fileDirName

      readWriteFile.writeDirAndFile(jsonData, dataFileDir, initialReadFile)

      // modReadData = readModUpdate.updateModData(jsonData);

      // modData.roomStays = readModUpdate.updateRoomStays(modReadData);
      // modData.resGuests = readModUpdate.updateResGuests(modReadData.resGuests);
      // modData.resGlobalInfo = modReadData.resGlobalInfo;

      const modData = readModUpdate.commandModData(jsonData)

      const initialModFile = fileDirName + '_mod.json'

      readWriteFile.writeDirAndFile(JSON.stringify(modData), dataFileDir, initialModFile)

      collectedModData.push(modData)
    }

    res.send(collectedModData)
  }

  doApiReadWriteNameMod (req, res, next) {
    const orderId = req.params.orderId
    //  let modData = {};
    const collectedModData = []
    let jsonData = {}

    let modData = {}

    const fileDirName = orderId

    const initialReadFile = orderId + '_read.json'

    const url = 'https://hotels.atdtravel.com/gateway/read/' + orderId

    Request.get(url, (error, response, body) => {
      if (error) {
        return console.log(error)
      }

      // jsonData = JSON.parse(body)
      jsonData = body

      const dataFileDir = '/Users/lancekesson/Projects/services-admin/data_output/' + fileDirName

      readWriteFile.writeDirAndFile(jsonData, dataFileDir, initialReadFile)

      // modReadData = readModUpdate.updateModData(jsonData);

      // modData.roomStays = readModUpdate.updateRoomStays(modReadData);
      // modData.resGuests = readModUpdate.updateResGuests(modReadData.resGuests);
      // modData.resGlobalInfo = modReadData.resGlobalInfo;

      modData = readModUpdate.commandModData(jsonData)

      const initialModFile = orderId + '_mod.json'
      readWriteFile.writeDirAndFile(JSON.stringify(modData), dataFileDir, initialModFile)

      collectedModData.push(modData)

      // send data to api
      res.send(collectedModData)
    })
  }

  doApiWriteNameMod (req, res, next) {
    const orderId = req.params.orderId

    let modData = {}

    const fileDirName = orderId

    const readFile = fileDirName + '_mod.json'

    const dataFileDir = '/Users/lancekesson/Projects/services-admin/data_output/' + fileDirName + '/' + readFile
console.log(dataFileDir)
    modData = readWriteFile.readFile(dataFileDir)

    const modUrl = 'https://hotels.atdtravel.com/gateway/bookings/modify/' + orderId
    console.log(modUrl)
    Request.post({
      headers: { 'content-type': 'application/json' },
      url: modUrl,
      body: JSON.stringify(modData)
    }, (error, response, body) => {
      if (error) {
        return console.log(error)
      }
 console.log('here');
      modData = body
      // send data to api
      res.send(modData)
    })

    // send data to api
    // res.send(modData)
  }
}

module.exports = new IbsModificationController()
