const readWriteFile = require('../../services/file.service.js')
const readModUpdate = require('../../services/ibs_modification/read_mod_update.service.js')
const path = require('path')
const Request = require('request')
const fs = require('fs')

class IbsModificationController {
  doReadWriteNameMod (req, res, next) {
    // variables
    const collectedModData = []

    const files = fs.readdirSync('../../tmp/')
    // let modData = {};

    for (const file of files) {
      const dataPath = '../../../tmp/' + file

      const jsonData = readWriteFile.readFile(dataPath)

      const fileDirName = path.parse(file).name

      const initialReadFile = fileDirName + '_read.json'

      const dataFileDir = '../../data/' + fileDirName

      readWriteFile.writeDirAndFile(jsonData, dataFileDir, initialReadFile)

      // modReadData = readModUpdate.updateModData(jsonData);

      // modData.roomStays = readModUpdate.updateRoomStays(modReadData);
      // modData.resGuests = readModUpdate.updateResGuests(modReadData.resGuests);
      // modData.resGlobalInfo = modReadData.resGlobalInfo;

      const modData = readModUpdate.commandModData(jsonData)

      const initialModFile = fileDirName + '_mod.json'

      readWriteFile.writeDirAndFile(JSON.stringify(modData), dataFileDir, initialModFile)

      collectedModData.push(modData)
    }

    res.send(collectedModData)
  }

  doApiReadWriteNameMod (req, res, next) {
    const orderId = req.query.order_id
    //  let modData = {};
    const collectedModData = []
    let jsonData = {}

    const dataPath = '../../../data/'
    const fileDirName = orderId

    const initialReadFile = orderId + '_read.json'

    Request.get('http://localhost:8099/gateway/read/:orderId', (error, response, body) => {
      if (error) {
        return console.log(error)
      }

      jsonData = JSON.parse(body)

      readWriteFile.writeDirAndFile(jsonData, fileDirName, initialReadFile)

      // modReadData = readModUpdate.updateModData(jsonData);

      // modData.roomStays = readModUpdate.updateRoomStays(modReadData);
      // modData.resGuests = readModUpdate.updateResGuests(modReadData.resGuests);
      // modData.resGlobalInfo = modReadData.resGlobalInfo;

      const modData = readModUpdate.commandModData(jsonData)

      const initialModFile = orderId + '_mod.json'
      readWriteFile.writeDirAndFile(JSON.stringify(modData), fileDirName, initialModFile)

      collectedModData.push(modData)

      // send data to api
      res.send(collectedModData)
    })
  }
}

module.exports = new IbsModificationController()
