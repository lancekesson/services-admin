class IbsModificationController {

    doReadWriteNameMod(req, res, next) {

        let files = fs.readdirSync('tmp/');
        let modData = {};

        for( file of files) {

            let dataPath = 'tmp/' + file;

            let jsonData = readWriteFile.readFile(
                dataPath
                );

            let fileDirName = path.parse(file).name;
    
            let initialReadFile = fileDirName + '_read.json'
    
            readWriteFile.writeDirAndFile(jsonData, fileDirName, initialReadFile)
    
            modReadData = readModUpdate.updateModData(jsonData);
    
            modData.roomStays = readModUpdate.updateRoomStays(modReadData);
            modData.resGuests = readModUpdate.updateResGuests(modReadData.resGuests);
            modData.resGlobalInfo = modReadData.resGlobalInfo;

            let initialModFile = fileDirName + '_mod.json';
            readWriteFile.writeDirAndFile(JSON.stringify(modData), fileDirName, initialModFile)

            collectedModData.push(modData);
        }
        

        res.send(collectedModData);
    }

}

module.exports = new IbsModificationController;