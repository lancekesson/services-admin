stockService = require('../services/stock.service');
orderProductService = require('../services/order_product.service');

class StockController {

    constructor(stockModel) {
        this.stockModel = stockModel;
    }

    async getStockReceipt(req, res, next) {
      const supplierOrderRef = req.params.supplierId;

      let stockReceipt = await stockService.getStockReceipt(supplierOrderRef);
      res.send(stockReceipt);

    }

    async getStockReceiptLines(req, res, next) {
      const stockReceiptId = req.params.stockReceiptId;

      let stockReceiptLines = await stockService.getStockReceiptLines(stockReceiptId);
      res.send(stockReceiptLines);

    }

    async getStockItems(req, res, next) {
      const stockReceiptLineId = req.params.stockReceiptLineId;

      let stockItems = await stockService.getStockItems(stockReceiptLineId);
      res.send(stockItems);

    }

    async getStockItemUsage(req, res, next) {
      const stockItemId = req.params.stockItemId;

      let stockItemUse = await stockService.getStockItemUse(stockItemId);
      res.send(stockItemUse);

    }

    async updateStock(req, res, next) {
      const orderId = req.params.orderId;
      let orderProduct = await orderProductService.getOrderProduct(orderId);


      res.send(orderProduct);

    }

    async addStockItemUsage(req, res, next) {

      const stockItemIdDto = stockService.buildStockItemUse(req.body);
console.log(stockItemIdDto);
      let stockItemUse = await stockService.addStockItemUse(stockItemIdDto);
      res.send(stockItemUse);

    }

    async getReturnLine(req, res, next) {

      const orderId = req.params.orderId;

      let returnLine = await stockService.getReturnLine(orderId);
      res.send(returnLine);

    }

    async addReturnLine(req, res, next) {

      const returnLineDto = stockService.buildReturnLine(req.body);
console.log(stockItemIdDto);
      let stockItemUse = await stockService.addReturnLine(returnLineDto);
      res.send(stockItemUse);

    }


}

module.exports = new StockController;