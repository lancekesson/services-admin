# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Services Admin panel
* Initial POC
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Setup
Clone repo
run react server
* Dependencies
There is requirement for it to be connected to the production environments

### IBS Modification Update Process
* 1. run read request in Postman - {{hotelServer}}/gateway/read/1714772
* 2. copy json output to file labelled {orderID}.json in the tmp dir - tmp/1714772.json
* 3. start the node js server - nodemon server.js
* 4. open browser and navigate to the readmod route on localhost with port that the server started on - localhost:3001/readmod
* 5. this will generate two files for each tmp/order json file - 1714772_mod.json and 1714772_read.json
* xxx_read.json is a backup of the initial read data
* xxx_mod.json is the data that needs to be checked, then placed as the body of the modification request.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact