const db = require('../../../services/db.service');
const StockItemUse = require('../../entities/stock/StockItemUse.entity');
const StockItemUseRead = require('../../entities/stock/StockItemUseRead.entity');

class StockItemUseRepository {
  constructor() {
    const ret = [];
  }

  findById(stockItemId, result) {
    db.query(`SELECT * FROM stock_item_uses WHERE stock_item_id = '${stockItemId}'`, (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log('found stock_item_use: ', res[0]);
        const repo = this.buildRepo(res);
        result(null, repo);
        return;
      }

      // not found Customer with the id
      result({ kind: 'not_found' }, null);
    });
  }

  create(stockItemDto, result) {
    if (stockItemDto instanceof StockItemUse) {
      db.query(
        'INSERT INTO stock_item_uses SET ?', stockItemDto, (err, res) => {
          if (err) {
            console.log('error: ', err);
            result(err, null);
            return;
          }

          if (res.insertId) {
            console.log(`successfully added ${res.insertId}`);
            result(null, { stock_Item_use_id: res.insertId });
            return;
          }

          // not found Customer with the id
          result({ kind: 'not_found' }, null);
        },
      );
    } else {
      // not found Customer with the id
      result({ error: 'input is not an instance of StockItemUse' }, null);
    }
  }

  buildRepo(res) {
    this.ret = res.map((i) => new StockItemUseRead(i));
    return this.ret;
  }
}

module.exports = new StockItemUseRepository();
