class CreatedDate {
  generateCreateDate() {
    const nowDate = new Date();
    return this.formatCreateDate(nowDate);
  }

  formatCreateDate(date) {
    this.createDate = date.toISOString().slice(0, 19).replace('T', ' ');
    return this.createDate;
  }
}

module.exports = CreatedDate;
