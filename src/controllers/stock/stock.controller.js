const stockService = require('../../services/stock.service')
const orderProductService = require('../../services/order_product.service')

class StockController {
  constructor (stockModel) {
    this.stockModel = stockModel
  }

  async getStockReceipt (req, res, next) {
    const supplierOrderRef = req.params.supplierId
    const stockReceipt = await stockService.getStockReceipt(supplierOrderRef)
    res.send(stockReceipt)
  }

  async addStockReceipt (req, res, next) {
    const stockReceiptDto = stockService.buildStockReceipt(req.body)
    const stockReceipt = await stockService.addStockReceipt(stockReceiptDto)
    res.send(stockReceipt)
  }

  async getStockReceiptLines (req, res, next) {
    const stockReceiptId = req.params.stockReceiptId
    const stockReceiptLines = await stockService.getStockReceiptLines(stockReceiptId)

    res.send(stockReceiptLines)
  }

  async addStockReceiptLine (req, res, next) {
    const stockReceiptLineDto = stockService.buildStockReceiptLine(req.body)
    const stockReceiptLine = await stockService.addStockReceiptLine(stockReceiptLineDto)

    res.send(stockReceiptLine)
  }

  async getStockItems (req, res, next) {
    const stockReceiptLineId = req.params.stockReceiptLineId
    const stockItems = await stockService.getStockItems(stockReceiptLineId)

    res.send(stockItems)
  }

  async addStockItem (req, res, next) {
    const stockItemDto = stockService.buildStockItem(req.body)
    const stockItem = await stockService.addStockItem(stockItemDto)

    res.send(stockItem)
  }

  async getStockItemUsage (req, res, next) {
    const stockItemId = req.params.stockItemId
    const stockItemUse = await stockService.getStockItemUse(stockItemId)

    res.send(stockItemUse)
  }

  async updateStock (req, res, next) {
    const orderId = req.params.orderId
    const orderProduct = await orderProductService.getOrderProduct(orderId)

    res.send(orderProduct)
  }

  async addStockItemUsage (req, res, next) {
    const stockItemIdDto = stockService.buildStockItemUse(req.body)
    const stockItemUse = await stockService.addStockItemUse(stockItemIdDto)

    res.send(stockItemUse)
  }

  async getReturnLine (req, res, next) {
    const orderId = req.params.orderId
    const returnLine = await stockService.getReturnLine(orderId)

    res.send(returnLine)
  }

  async addReturnLine (req, res, next) {
    const returnLineDto = stockService.buildReturnLine(req.body)
    const returnLine = await stockService.addReturnLine(returnLineDto)

    res.send(returnLine)
  }
}

module.exports = new StockController()
