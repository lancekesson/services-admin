const fs = require('fs')

const fileService = {

  // helper methods
  readFile: (filePath, encoding = 'utf8') => {
    const file = fs.readFileSync(filePath, encoding)
    return file
  },


  // async function readFile(path) {
  //   return new Promise((resolve, reject) => {
  //     fs.readFile(path, 'utf8', function (err, data) {
  //       if (err) {
  //         reject(err);
  //       }
  //       resolve(data);
  //     });
  //   });
  // }

  writeFile: function (fileData, filePath = '', encoding = 'utf8') {
    fs.writeFileSync(filePath, fileData)
  },

  writeDirAndFile: function (fileData, fileDir = '', fileName = '') {
    const filePath = fileDir + '/' + fileName

    if (fs.existsSync(fileDir)) {
      this.writeFile(fileData, filePath)
    } else {
      fs.mkdirSync(fileDir)
      this.writeFile(fileData, filePath)
    }
  }
}

module.exports = fileService
