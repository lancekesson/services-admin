orderProductModel = require('../models/repositories/order_product/order_product.respository');

class OrderProductService {

    constructor() {

    }

    async getOrderProduct(orderId) {

        const orderProducts = await this.getOrderProductData(orderId);
        const filteredOrderProducts = this.filterOrderProducts(orderProducts);
        return filteredOrderProducts;

    }

    filterOrderProducts(orderProducts) {

        let obj = {};

        let filteredOrderProducts = new Map();
        for (let orderProduct of orderProducts) {
            if (filteredOrderProducts.has(orderProduct.nid)) {
                let tmp = filteredOrderProducts.get(orderProduct.nid);
                tmp.push(orderProduct);
                filteredOrderProducts.set(orderProduct.nid, tmp);
            } else {
                filteredOrderProducts.set(orderProduct.nid, [orderProduct]);
            }
        }

        filteredOrderProducts.forEach((value, key) => {
            obj[key] = value
        });

        return JSON.stringify(obj);
    }

    async getOrderProductData(confId) {

        return await new Promise(function (resolve, reject) {
            orderProductModel.getOrderProductByConf(confId, (err, data) => {

                if (err) {
                    if (err.kind === "not_found") {
                        console.log(`Not found Order Product with conf id ${confId}.`);
                        reject(err);
                    } else {
                        console.log("Error retrieving Order Product with conf id " + confId);
                        reject(err);
                    }
                } else {
                    resolve(data);
                }
            });
        });
    }



}

module.exports = new OrderProductService;